import RPi.GPIO as GPIO                                               # Importation des librairies qui gerent les ports
import time                                                           # Importation de la librairie temps
import requests 

GPIO.setmode(GPIO.BCM) 

trig = 19
echo = 26                                               # BCM : Numero des GPIO (GPIO 18)
GPIO.setup(trig, GPIO.OUT)
GPIO.setup(echo, GPIO.IN)# Definition du port en sortie
GPIO.setwarnings(False)                                               # Mettre sur OFF les alertes (qui sont inutiles)


# Affichage de texte
print("\n+------------------/ rasp capteur de distance /---------------------------+")
print("|                                                 	    |")
print("| |")
print("|                                                 	    |")
print("+----------------------------------------------------------+\n")

#i = 0                                                                  # Definition d'une variable type compteur
place_occupe = False 

while True :
    GPIO.output(trig, True)
    time.sleep(0.0001)
    GPIO.output(trig, False)

    while (GPIO.input(echo) == False) :
        start = time.time()
    while (GPIO.input(echo) == True) :
        end = time.time()
        
        
    d_time = end - start

    distance = d_time / 0.000058

    print("Distance {} cm".format (distance))

    if distance > 8.0 and distance < 18.0 :
        if place_occupe == False :
            requests.post("http://gdumont.ddns.net:5000/api/parking/occupied/5c44432443ab9a3048eed5e4",json = {"occupied":True})
            print (str("true sended"))
            time.sleep(60)
        place_occupe = True
        
    else :
        if place_occupe == True :
            requests.post("http://gdumont.ddns.net:5000/api/parking/occupied/5c44432443ab9a3048eed5e4",json = {"occupied":False})
            print (str("false sended"))
            time.sleep(60)
        place_occupe = False
        
    time.sleep(3)
    

# GO.cleanup()